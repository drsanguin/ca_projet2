#include <Basic_block.h>


//static
void Basic_block::show_dependances(Instruction *i1, Instruction *i2){
   
   if(i1->is_dep_RAW1(i2)) 
      cout<<"Dependance i"<<i1->get_index()<<"->i"<<i2->get_index()<<": RAW1"<<endl; 
   if(i1->is_dep_RAW2(i2)) 
      cout<<"Dependance i"<<i1->get_index()<<"->i"<<i2->get_index()<<": RAW2"<<endl;
   
   if(i1->is_dep_WAR(i2)) 
      cout<<"Dependance i"<<i1->get_index()<<"->i"<<i2->get_index()<<": WAR"<<endl;
   
   if(i1->is_dep_WAW(i2)) 
      cout<<"Dependance i"<<i1->get_index()<<"->i"<<i2->get_index()<<": WAW"<<endl;
   
   if(i1->is_dep_MEM(i2)) 
      cout<<"Dependance i"<<i1->get_index()<<"->i"<<i2->get_index()<<": MEM"<<endl;
   
}

Basic_block::Basic_block(){
   _head = NULL;
   _end = NULL;
   _branch = NULL;
   _index = 0;
   _nb_instr = 0;
   _firstInst=NULL;
   _lastInst=NULL;
   dep_done = false;
   use_def_done = false;
   
   for(int i=0; i<NB_REG; i++){
      Use[i]= false;
      LiveIn[i] = false;
      LiveOut[i] = false;
      Def[i] = false;
      DefLiveOut[i] = -1;
   }

 for(int i=0; i<NB_MAX_BB; i++){
      Domin[i]= true;
 }

}


Basic_block::~Basic_block(){}


void Basic_block::set_index(int i){
   _index = i;
}

int Basic_block::get_index(){
   return _index;
}

void Basic_block::set_head(Line *head){
   _head = head;
}

void Basic_block::set_end(Line *end){
   _end = end;
}

Line* Basic_block::get_head(){
   return _head;
}

Line* Basic_block::get_end(){
   return _end;
}

void Basic_block::set_successor1(Basic_block *BB){
   _succ.push_front(BB);
}

Basic_block *Basic_block::get_successor1(){
   if (_succ.size()>0)
      return _succ.front();
   else 
      return NULL;
}

void Basic_block::set_successor2(Basic_block *BB){	
   _succ.push_back(BB);
}

Basic_block *Basic_block::get_successor2(){
   if (_succ.size()> 1)
      return _succ.back();
   else 
      return NULL;
}

void Basic_block::set_predecessor(Basic_block *BB){
   _pred.push_back(BB);
}

Basic_block *Basic_block::get_predecessor(int index){

   list<Basic_block*>::iterator it;
   it=_pred.begin();
   int size=(int)_pred.size();
   if(index< size){
      for (int i=0; i<index; i++, it++);
      return *it;	
   }
   else cout<<"Error: index is bigger than the size of the list"<<endl; 	
   return _pred.back();
	
}

int Basic_block::get_nb_succ(){
   return _succ.size();
}

int Basic_block::get_nb_pred(){
   return _pred.size();
}

void Basic_block::set_branch(Line* br){
   _branch=br;
}

Line* Basic_block::get_branch(){
   return _branch;
}

void Basic_block::display(){
   cout<<"Begin BB"<<endl;
   Line* element = _head;
   int i=0;
   if(element == _end)	
      cout << _head->get_content() <<endl;
  
   while(element != _end->get_next()){
      if(element->isInst()){
	 cout<<"i"<<i<<" ";
	 i++;
      }
      if(!element->isDirective())
	 cout <<element->get_content() <<endl;
      
      element = element->get_next();
   }
   cout<<"End BB"<<endl;
}

int Basic_block::size(){
   Line* element = _head;
   int lenght=0;
   while(element != _end){
      lenght++;
      if(element->get_next()==_end)	
	 break;
      else 
	 element = element->get_next();
   }
   return lenght;
}	


void Basic_block::restitution(string const filename){	
   Line* element = _head;
   ofstream monflux(filename.c_str(), ios::app);
   if(monflux){
      monflux<<"Begin BB"<<endl;
      if(element == _end)	
	monflux << _head->get_content() <<endl;
      while(element != _end)
      {
	 if(element->isInst()) 
	    monflux<<"\t";
	 if(!element->isDirective())
	    monflux << element->get_content()<<endl ;
		
	 if(element->get_next()==_end){
	    if(element->get_next()->isInst()) 
	       monflux<<"\t";
	    if(!element->isDirective())
	       monflux << element->get_next()->get_content()<<endl;
	    break;
	 }
	 else element = element->get_next();
      }
      monflux<<"End BB\n\n"<<endl;		
   }
   else {
      cout<<"Error cannot open the file"<<endl;
   }
   monflux.close();

}

bool Basic_block::is_labeled(){
   if (_head->isLabel()){
      return true;
   }
   else return false;
}

int Basic_block::get_nb_inst(){   
   if (_nb_instr == 0)
      link_instructions();
   return _nb_instr;
    
}

Instruction* Basic_block::get_instruction_at_index(int index){
   Instruction *inst;
   
   if(index >= get_nb_inst()){
      return NULL;
   }
   
   inst=get_first_instruction();

   for(int i=0; i<index; i++, inst=inst->get_next());

   return inst;
}

Line* Basic_block::get_first_line_instruction(){
   
   Line *current = _head;
   while(!current->isInst()){
      current=current->get_next();
      if(current==_end)
	 return NULL;
   }
   return current;
}

Instruction* Basic_block::get_first_instruction(){
  if(_firstInst==NULL){
      _firstInst= getInst(this->get_first_line_instruction());
      this->link_instructions();
  }
   return _firstInst;
}

Instruction* Basic_block::get_last_instruction(){
   if(_lastInst==NULL)
      this->link_instructions();
   return _lastInst;
}


/* link_instructions  num�rote les instructions du bloc */
/* remplit le champ nb d'instructions du bloc (_nb_instr) */
/* remplit le champ derniere instruction du bloc (_lastInst) */
void Basic_block::link_instructions(){

   int index=0;
   Line *current, *next;
   current=get_first_line_instruction();
   next=current->get_next();

   Instruction *i1 = getInst(current);

   i1->set_index(index);
   index++;
   Instruction *i2;
   
//Calcul des successeurs
   while(current != _end){
   
      while(!next->isInst()){
	 next=next->get_next();
	 if(next==_end){
	    if(next->isInst())
	       break;
	    else{
	       _lastInst = i1;
	       _nb_instr = index;
	       return;
	    }
	 }
      }
      
      i2 = getInst(next);
      i2->set_index(index);
      index++;
      i1->set_link_succ_pred(i2);
      
      i1=i2;
      current=next;
      next=next->get_next();
   }
   _lastInst = i1;
   _nb_instr = index;
}

bool Basic_block::is_delayed_slot(Instruction *i){
   if (get_branch()== NULL)
      return false;
   int j = (getInst(get_branch()))->get_index();
   return (j < i-> get_index());

}

/* set_link_succ_pred : ajoute succ comme successeur de this et ajoute this comme pr�d�cesseur de succ
 */

void Basic_block::set_link_succ_pred(Basic_block* succ){
  _succ.push_back(succ);
  succ->set_predecessor(this);
}

/* add_dep_link ajoute la d�pendance avec pred � la liste des dependances pr�c�desseurs de succ */
/* ajoute la dependance avec succ � la liste des d�pendances successeurs de pred */

/* dep est une structure de donn�es contenant une instruction et  un type de d�pendance */

void add_dep_link(Instruction *pred, Instruction* succ, t_Dep type){
   dep *d;
   d=(dep*)malloc(sizeof(dep));
   d->inst=succ;
   d->type=type;
   pred->add_succ_dep(d);
   
   d=(dep*)malloc(sizeof(dep));
   d->inst=pred;
   d->type=type;
   succ->add_pred_dep(d);
}


/* calcul des d�pendances entre les instructions dans le bloc  */
/* une instruction a au plus 1 reg dest et 2 reg sources */
/* Attention le reg source peut �tre 2 fois le m�me */ 
/* Utiliser les m�thodes is_dep_RAW1, is_dep_RAW2, is_dep_WAR, is_dep_WAW, is_dep_MEM pour d�terminer les d�pendances */

/* ne pas oublier les d�pendances de controle avec le branchement s'il y en a un */

/* utiliser la fonction add_dep_link ci-dessus qui ajoute � la liste des d�pendances pred et succ une dependance entre 2 instructions */

void Basic_block::comput_pred_succ_dep(){
   
	// IMPORTANT : laisser les 2 instructions ci-dessous 
	link_instructions();
	if (dep_done) return;

	for(int i = _nb_instr - 1; i >= 0; i--)
	{
		Instruction* inst = get_instruction_at_index(i);
		bool has_WAR = false;

		for(int j = i-1; j >= 0; j--)
		{
			if(get_instruction_at_index(j)->is_dep_RAW1(inst)) {
				add_dep_link(get_instruction_at_index(j), inst, RAW);
				break;
			}
				
		}

		for(int j = i-1; j >= 0; j--)
		{

			if(get_instruction_at_index(j)->is_dep_RAW2(inst)) {
				add_dep_link(get_instruction_at_index(j), inst, RAW);
				break;
			}
		}

		for(int j = i-1; j >= 0; j--)
		{
			if(get_instruction_at_index(j)->is_dep_WAR1(inst) || get_instruction_at_index(j)->is_dep_WAR2(inst)) {
				add_dep_link(get_instruction_at_index(j), inst, WAR);
				has_WAR = true;
				break;
			}
		}

		if( ! has_WAR)
		{
			for(int j = i-1; j >= 0; j--)
			{
				if(get_instruction_at_index(j)->is_dep_WAW(inst))
				{
					add_dep_link(get_instruction_at_index(j), inst, WAW);
					break;
				}
			}
		}

		if(inst->is_mem())
		{	
			for(int j = i-1; j >= 0; j--)
			{
				if( ! get_instruction_at_index(j)->is_mem())
					continue;
				
				if(get_instruction_at_index(j)->is_mem_load() && inst->is_mem_load())
					continue;
				
				if(inst->get_reg_src2() == get_instruction_at_index(j)->get_reg_src2())
					continue;

				add_dep_link(get_instruction_at_index(j), inst, MEMDEP);
			}
		}
	}

	if(getInst(_end)->is_nop())
		for(int i = 0; i < _nb_instr - 2; i++)
		{
			Instruction* inst = get_instruction_at_index(i);
		
			if(inst->get_nb_succ() == 0)
				add_dep_link(inst, getInst(_end->get_prev()), CONTROL);
		}
		
	
	// NE PAS ENLEVER : cette fonction ne doit �tre appel�e qu'une seule fois
	dep_done = true;
	return;
}



void Basic_block::reset_pred_succ_dep(){

  Instruction *ic=get_first_instruction();
  while (ic){
    ic -> reset_pred_succ_dep();
    ic = ic -> get_next();
  }
  dep_done = false;
  return;
}


/* calcul le nb de cycles pour executer le BB, on suppose qu'une instruction peut sortir du pipeline � chaque cycle, il faut trouver les cycles de gel induit par les d�pendances */

int Basic_block::nb_cycles(){
  
	int gelTemp, gelMax ,nmb=_nb_instr, gelFinal =0;
	Instruction  *inst, *inst2;
	 /*** A COMPLETER ***/
	 for (int i=1; i < nmb ; i++ )
	 {
		inst = get_instruction_at_index(i);
		gelMax=gelTemp = 0;
		int nb_pred =inst->get_nb_pred();
		for(int j=0; j <nb_pred ; j++)
		{
			dep* dependance = inst->get_pred_dep(j);
			inst2 = dependance->inst;
			if(dependance->type==RAW)
			{
				gelTemp= delai(inst2->get_type(),inst->get_type())-i+inst2->get_index();
			}
				  
				 if (gelTemp>gelMax) {
				  	gelMax = gelTemp;
				  }
		}
		gelFinal+=gelMax;  
	} 
	return nmb+ gelFinal;
}

/* 
calcule DEF et USE pour l'analyse de registre vivant 
� la fin on doit avoir
 USE[i] vaut 1 si $i est utilis� dans le bloc avant d'�tre potentiellement d�fini dans le bloc
 DEF[i] vaut 1 si $i est d�fini dans le bloc 
******************/

void Basic_block::compute_use_def(void){

  /*** A COMPLETER ***/
	Instruction* inst, *inst2;
	int index;
	for (int i=0; i< _nb_instr; i++) {
			inst = get_instruction_at_index(i);
			if(inst->is_nop())
			{
				Use[0]=true; // Fin du bloc
				inst2 = inst->get_prev();
				if(inst2->is_indirect_branch()) {
					index = inst2->get_reg_src1()->get_reg();
				if(index==31) {
					Def[2] = true;	
				}
			}

				}
				else {
					if(inst->is_mem()) {
						if(inst->is_mem_load()){
							Def[inst->get_reg_dst()->get_reg()]=true;
							if(inst->get_reg_src1()!=NULL){
								index = inst->get_reg_src1()->get_reg();
								if(!Def[index])
									Use[index]=true;
							}
							}
							else {
								if(inst->get_reg_src1()!=NULL){
									index = inst->get_reg_src1()->get_reg();
									if(!Def[index])
										Use[index]=true;
									index = inst->get_reg_src2()->get_reg();
									if(!Def[index])
										Use[index]=true;
								}
						}
					}
						else if (inst->is_branch()) {
							if(inst->is_call()) {
								Def[31]=true;
								Use[4]=Use[5]=Use[6]=true;
							}
							else if(inst->is_cond_branch()) {
									if(inst->get_reg_src1()!=NULL) {
										index = inst->get_reg_src1()->get_reg();
										if(!Def[index])
											Use[index]=true;
									}
									
									if(inst->get_reg_src2()!=NULL){
										index = inst->get_reg_src2()->get_reg();
										if(!Def[index])
											Use[index]=true;	
											}
							}
									else {
										if(inst->is_indirect_branch()) { 
										index = inst->get_reg_src1()->get_reg();
										if(!Def[index])
											Use[index]=true;
										}
								}
							}
							 else {
								if(inst->get_type()==ALU) {
									Def[inst->get_reg_dst()->get_reg()]=true;
									
									if(inst->get_reg_src1()!=NULL){
										index = inst->get_reg_src1()->get_reg();
										if(!Def[index])
											Use[index]=true;
									}
									
									if(inst->get_reg_src2()!=NULL){
										index = inst->get_reg_src2()->get_reg();
										if(!Def[index])
											Use[index]=true;	
										}
									}
								 else if (inst->get_type()==OTHER || inst->get_type()==BAD) {
									continue; // Do nothing
									} 
					} 
		}
	}
    return;
}

/**** compute_def_liveout 
� la fin de la fonction on doit avoir
DefLiveOut[i] vaut l'index de l'instruction du bloc qui d�finit $i si $i vivant en sortie seulement
Si $i est d�fini plusieurs fois c'est l'instruction avec l'index le plus grand
*****/
void Basic_block::compute_def_liveout(){
  /*** A COMPLETER ****/
	Instruction* inst;
	for(int i=0; i<NB_REG; i++){
				if(LiveOut[i]){ 
				for(int j=_nb_instr-1; j>-1;j--) {
					inst= get_instruction_at_index(j);
					if(inst->get_reg_dst()!=NULL){
						if(inst->get_reg_dst()->get_reg()==i){
							if (DefLiveOut[i]<j)
								{
									DefLiveOut[i]=j;
									break;
								}
							}
						}
					}
		} 
	}
	// Cas d'une fonction
	int index;
	inst =	get_instruction_at_index(_nb_instr-1);
	if(inst->is_nop()) {
		if(inst->get_prev()->is_indirect_branch()) {
			index= inst->get_reg_src1()->get_reg();
			if(index==31) {
				DefLiveOut[2] =  _nb_instr-2;
				}
			}
		}
}

list<int> Basic_block::compute_renommables()
{
	list<int> renommables;

	//cout << "renommables: " << endl;

	for(int i = 0; i < NB_REG; i++)
	{
		//cout << "Def[" << i << "] = " << Def[i] << " && " << "LiveOut[" << i << "] = " << LiveOut[i] << " Use[" << i << "] = " << Use[i] <<endl;
		if(Def[i] && !LiveOut[i])
		{
			//cout << "	" << i << endl;
			renommables.push_back(i);
		}
	}

	return renommables;
}

list<int> Basic_block::compute_frees()
{
	list<int> frees;

	//cout << "frees: " << endl;

	for(int i = 1; i < NB_REG; i++)
	{
		if(!Def[i] && !LiveOut[i])
		{
			//cout << "	" << i << endl;
			frees.push_back(i);
		}
	}

	return frees;
}

/**** renomme les registres renommables en utilisant comme registres disponibles ceux dont le num�ro est dans la liste param�tre 
*****/
void Basic_block::reg_rename(list<int> *frees){
	if(frees->size() <= 0)
		return;

	list<int> renommables = compute_renommables();

	for(list<int>::iterator it = renommables.begin(); it != renommables.end(); it++)
	{
		if(frees->size() <= 0)
			return;

		int renommable = *it;
		int free = *(frees->begin());
		frees->remove(free);
		
		bool first_done = false;

		for(int i = 0; i < _nb_instr; i++)
		{
			Instruction* inst = get_instruction_at_index(i);

			if(inst->get_reg_src1() != NULL)
			{
				int reg = inst->get_reg_src1()->get_reg();

				if(reg == renommable)
					inst->get_reg_src1()->set_reg(free);
			}

			if(inst->get_reg_src2() != NULL)
			{
				int reg = inst->get_reg_src2()->get_reg();

				if(reg == renommable)
					inst->get_reg_src2()->set_reg(free);
			}

			if(inst->get_reg_dst() != NULL)
			{
				int reg = inst->get_reg_dst()->get_reg();

				if(reg == renommable)
				{
					if(!first_done)
					{
						inst->get_reg_dst()->set_reg(free);
						first_done = true;
					}
					else
					{
						free = *(frees->begin());
						frees->remove(free);

						inst->get_reg_dst()->set_reg(free);
					}
				}
			}
		}
	}
}


/**** renomme les registres renommables en utilisant comme registres disponibles ceux disponible pour le bloc d'apr�s l'analyse de vivacit� et def-use

*****/
void Basic_block::reg_rename(){
	list<int> frees = compute_frees();
	
	reg_rename(&frees);
}


void Basic_block::apply_scheduling(list <Node_dfg*> *new_order){
   list <Node_dfg*>::iterator it=new_order->begin();
   Instruction *inst=(*it)->get_instruction();
   Line *n=_head, *prevn=NULL;
   Line *end_next = _end->get_next();
   if(!n){
      cout<<"wrong bb : cannot apply"<<endl;
      return;
   }
   
   while(!n->isInst()){
     prevn=n;
     n=n->get_next();
     if(n==_end){
       cout<<"wrong bb : cannot apply"<<endl;
       return;
     }
   }
   
   //y'a des instructions, on sait pas si c'est le bon BB, mais on va supposer que oui
   inst->set_index(0);
   inst->set_prev(NULL);
   _firstInst = inst;
   n = inst;
   
   if(prevn){
     prevn->set_next(n);
     n->set_prev(prevn);
   }
   else{
     set_head(n);
   }

   int i;
   it++;
   for(i=1; it!=new_order->end(); it++, i++){

     inst->set_link_succ_pred((*it)->get_instruction());
     
     inst=(*it)->get_instruction();
     inst->set_index(i);
     prevn = n;
     n = inst;
     prevn->set_next(n);
     n->set_prev(prevn);
     }
   inst->set_next(NULL);
   _lastInst = inst;
   set_end(n);
   n->set_next(end_next);
   return;
}

/* permet de tester des choses sur un bloc de base, par exemple la construction d'un DFG, � venir ... l� ne fait rien qu'afficher le BB */
void Basic_block::test(){
   cout << "test du BB " << get_index() << endl;
   display();
   cout << "nb de successeur : " << get_nb_succ() << endl;
   int nbsucc = get_nb_succ() ;
   if (nbsucc >= 1 && get_successor1())
      cout << "succ1 : " << get_successor1()-> get_index();
   if (nbsucc >= 2 && get_successor2())
      cout << " succ2 : " << get_successor2()-> get_index();
   cout << endl << "nb de predecesseurs : " << get_nb_pred() << endl ;
  
   int size=(int)_pred.size();
   for (int i = 0; i < size; i++){
      if (get_predecessor(i) != NULL)
	 cout << "pred "<< i <<  " : " << get_predecessor(i)-> get_index() << "; ";
   }
   cout << endl;
}
